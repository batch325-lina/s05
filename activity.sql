USE classic_models;

--
-- SELECT CUSTOMER WHERE country = ?
--
SELECT customerName FROM customers WHERE country = 'philippines';

--
-- SELECT contacName and firstname where name = ?;
--

SELECT contactLastName, contactFirstName FROM customers WHERE customerName = 'La Rochelle Gifts';

--
-- SELECT product name and msrp  WHERE product = ?;
--

SELECT productName, MSRP FROM products WHERE productName = 'The Titanic';

--
-- SELECT first and last WHERE emplyee = ?;
--

SELECT firstName, lastName FROM employees WHERE email = 'jfirrelli@classicmodelcars.com';

--
-- SELECT customer WHERE state is null;
--

SELECT customerName FROM customers WHERE state IS NULL;

--
-- SELECT first name and last name WHERE first name = ? AND last name = ?;
--

SELECT firstName, lastName FROM employees WHERE lastname = 'Patterson' AND firstName = 'Steve';

--
--SELECT customer name country and credit limit WHERE NOT country 'USA' and creditLimit > 3000;
--

SELECT customerName, country, creditLimit FROM customers WHERE NOT country = 'USA' AND creditLimit > 3000;

--
-- SELECT customer number WHERE comment LIKE '%DHL%';
--

SELECT customerNumber FROM orders WHERE comments LIKE '%DHL%';

--
-- SELECT prodcut line where description LIKE ?;
--

SELECT productLine FROM productlines WHERE textDescription LIKE '%state of the art%';

--
-- SELECT DISTINCT country
--

SELECT DISTINCT country FROM customers;

--
-- SELECT DISTINCT status
--

SELECT DISTINCT status FROM orders;

--
-- SELECT customerName WHERE countries = USA France Or Canada;
--

SELECT customerName FROM customers WHERE country = 'USA' OR country = 'France' OR country = 'Canada';

--
-- SELECT first name last name and office city  WHERE office = ?;
--

SELECT a.firstName, a.lastName, b.city FROM employees as a INNER JOIN offices as b ON a.officeCode = b.officeCode WHERE b.city = 'tokyo';

--
-- SELECT customer name WHERE eomployee = ?;
--

SELECT a.customerName FROM customers as a INNER JOIN employees as b ON a.salesRepEmployeenumber = b.employeeNumber WHERE firstName = 'leslie' AND lastName = 'Thompson';

--
-- SELECT product name and customername  WHERE order = ?;
--

SELECT a.productName, b.customerName FROM products as a INNER JOIN orderdetails as c ON a.productCode = c.productCode INNER JOIN orders as d ON c.orderNumber = d.orderNumber INNER JOIN customers as b ON b.customerNumber = d.customerNumber WHERE customerName = 'baane mini imports';

--
-- SELECT emplyee first name employee lastname customers name and office country WHERE custmer and office country = same;
--

SELECT a.firstName, a.lastName, b.customerName, c.country FROM employees as a INNER JOIN offices as c ON a.officeCode = c.officeCode INNER JOIN customers as b ON a.employeeNumber = b.salesRepEmployeenumber WHERE b.country = c.country;

--
-- SELECT prodcutname and quantity in stock WHERE product Line = plane;
--

SELECT productName, quantityInStock FROM products WHERE productLine = 'planes' AND quantityInStock < 1000;

--
-- SELECT customerName WHERE Number Like '%+81%;
--

SELECT customerName FROM customers WHERE phone LIKE '%+81%';